--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : vga.vhf
-- /___/   /\     Timestamp : 11/20/2015 10:32:41
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: C:\Xilinx\14.7\ISE_DS\ISE\bin\nt64\unwrapped\sch2hdl.exe -intstyle ise -family spartan6 -flat -suppress -vhdl vga.vhf -w "C:/Xilinx/Koncar - Projekti/VGA_Controller/vga.sch"
--Design Name: vga
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity vga is
   port ( pixel_clk : in    std_logic; 
          rst       : in    std_logic; 
          blue      : out   std_logic_vector (7 downto 0); 
          clk_out   : out   std_logic; 
          green     : out   std_logic_vector (7 downto 0); 
          h_sync    : out   std_logic; 
          n_blank   : out   std_logic; 
          n_sync    : out   std_logic; 
          red       : out   std_logic_vector (7 downto 0); 
          save_en   : out   std_logic; 
          v_sync    : out   std_logic);
end vga;

architecture BEHAVIORAL of vga is
   signal XLXN_1    : std_logic;
   signal XLXN_2    : integer;
   signal XLXN_3    : integer;
   component vga_controller
      port ( clk       : in    std_logic; 
             rst   : in    std_logic; 
             h_sync    : out   std_logic; 
             v_sync    : out   std_logic; 
             disp_ena  : out   std_logic; 
             column    : out   integer; 
             row       : out   integer; 
             n_blank   : out   std_logic; 
             clk_out   : out   std_logic; 
             save_en   : out   std_logic; 
             n_sync    : out   std_logic);
   end component;
   
   component background_test
      port ( display_enable : in    std_logic; 
             row            : in    integer; 
             column         : in    integer; 
             red            : out   std_logic_vector (7 downto 0); 
             green          : out   std_logic_vector (7 downto 0); 
             blue           : out   std_logic_vector (7 downto 0));
   end component;
   
begin
   XLXI_1 : vga_controller
      port map (clk=>pixel_clk,
                rst=>rst,
                clk_out=>clk_out,
                column=>XLXN_3,
                disp_ena=>XLXN_1,
                h_sync=>h_sync,
                n_blank=>n_blank,
                n_sync=>n_sync,
                row=>XLXN_2,
                save_en=>save_en,
                v_sync=>v_sync);
   
   XLXI_2 : background_test
      port map (column=>XLXN_3,
                display_enable=>XLXN_1,
                row=>XLXN_2,
                blue(7 downto 0)=>blue(7 downto 0),
                green(7 downto 0)=>green(7 downto 0),
                red(7 downto 0)=>red(7 downto 0));
   
end BEHAVIORAL;


