----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:53:01 11/19/2015 
-- Design Name: 
-- Module Name:    VGA_Controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vga_controller is
generic
(
	h_pulse  :  integer   := 120;		-- Sirina horizontalnog sinhronizacionog pulsa
   h_bp     :  integer   := 64;   	-- Horizontalni BACK PORCH (sirina u pikselima)
   h_pixels :  integer   := 800;  	-- Broj piksela - HORIZONTALNO
   h_fp     :  integer   := 56;   	-- Horizontalni FRONT PORCH (sirina u pikselima)
   h_pol    :  std_logic := '1';   	-- Polaritet horizontalnog sinhronizacionog pulsa
   v_pulse  :  integer   := 6;     	-- Sirina vertikalnog sinhronizacionog pulsa
   v_bp     :  integer   := 23;    	-- Vertikalni BACK PORCH (sirina u pikselima)
   v_pixels :  integer   := 600;  	-- Broj piksela - VERTIKALNO
   v_fp     :  integer   := 37;     -- Vertikalni FRONT PORCH (sirina u pikselima)
   v_pol    :  std_logic := '1'  	-- Polaritet vertikalnog sinhronizacionog pulsa
);
port
(
	clk 		 :  in   std_logic;  	
   rst       :  in   std_logic;  	
   h_sync    :  out  std_logic;  	
   v_sync    :  out  std_logic;  	
   disp_ena  :  out  std_logic;  	
   column    :  out  integer;    	
   row       :  out  integer;    	
   n_blank   :  out  std_logic;  	
	clk_out   :  out  std_logic;  
	save_en   :	 out  std_logic; 
   n_sync    :  out  std_logic  		
);
end vga_controller;

architecture behavior of vga_controller is
  constant  h_period  :  integer := h_pulse + h_bp + h_pixels + h_fp;  
  constant  v_period  :  integer := v_pulse + v_bp + v_pixels + v_fp;  
begin

	n_blank <= '1'; 
	n_sync <= '0';   
	save_en <= '1';   -- Obavezno mora biti ukljuceno
  
	process(clk, rst)
		variable h_count  :  integer range 0 to h_period - 1 := 0;  
		variable v_count  :  integer range 0 to v_period - 1 := 0;  
	begin
  
		clk_out <= clk;
  
		if (rst = '0') then  
			h_count := 0;         
			v_count := 0;         
			h_sync <= not h_pol;  
			v_sync <= not v_pol;  
			disp_ena <= '0';      
			column <= 0;         
			row <= 0;            
		elsif (rising_edge(clk)) then
			if(h_count < h_period - 1) then   
				h_count := h_count + 1;
			else
				h_count := 0;
				if(v_count < v_period - 1) then  
					v_count := v_count + 1;
				else
					v_count := 0;
				end if;
			end if;

			-- Manipulacija sa horizontalnim sinhronizacionim pulsom
			if(h_count < h_pixels + h_fp or h_count > h_pixels + h_fp + h_pulse) then
				h_sync <= not h_pol;    
			else
				h_sync <= h_pol;        
			end if;
			-- Manipulacija sa vertikalnim sinhronizacionim pulsom
			if(v_count < v_pixels + v_fp or v_count > v_pixels + v_fp + v_pulse) then
				v_sync <= not v_pol;   
			else
				v_sync <= v_pol;       
			end if;
      
		   -- Prikaz na ekran
			if(h_count < h_pixels and v_count < v_pixels) then  	
				disp_ena <= '1';                                  
				column <= h_count;
				row <= v_count; 
			else                                                
				disp_ena <= '0';                                  	
			end if;

		end if;
	end process;
end behavior;

