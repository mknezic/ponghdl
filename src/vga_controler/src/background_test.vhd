----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:45:39 11/19/2015 
-- Design Name: 
-- Module Name:    background_test - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity background_test is
Port (
	display_enable		: in std_logic;
	row 					: in integer;
	column 				: in integer;
	red					: out std_logic_vector (7 downto 0);
	green					: out std_logic_vector (7 downto 0);
	blue					: out std_logic_vector (7 downto 0)
);
end background_test;

architecture Behavioral of background_test is

begin
process (display_enable, row, column)
begin

	if (display_enable = '1') then
		if (row < 200) then
			red <= "11111111";
			green <= "00000000";
			blue <= "00000000";
		elsif (row < 400) then
			red <= "00000000";
			green <= "00000000";
			blue <= "11111111";
		else
			red <= "11111111";
			green <= "11111111";
			blue <= "11111111";
		end if;
	else
			red <= "00000000";
			green <= "00000000";
			blue <= "00000000";
	end if;
end process;

end Behavioral;

