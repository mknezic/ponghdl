----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:38:55 11/24/2015 
-- Design Name: 
-- Module Name:    game_draw - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity game_draw is
generic
(
	screen_width	:	integer := 800;
	screen_height	:	integer := 600;
	
	bg_red			:	std_logic_vector (7 downto 0) := "10011001";
	bg_green			:	std_logic_vector (7 downto 0) := "10011001";
	bg_blue			:	std_logic_vector (7 downto 0) := "10011001";
	
	paddle_red		:	std_logic_vector (7 downto 0) := "00000000";
	paddle_green	:	std_logic_vector (7 downto 0) := "11111111";
	paddle_blue		:	std_logic_vector (7 downto 0) := "00000000";
	
	ball_red			:	std_logic_vector (7 downto 0)	:= "00000000";
	ball_green		:	std_logic_vector (7 downto 0) := "00000000";
	ball_blue		:	std_logic_vector (7 downto 0) := "11111111";
	
	paddle_width	:	integer 	:= 100;
	paddle_height	:	integer 	:= 20;
	
	ball_dim		:	integer	:= 20
);
Port
(
	display_enable	:	in 	std_logic;
	row				:	in		integer;
	column			:	in		integer;
	
	paddle_x			:	in		integer;
	paddle_y			:	in		integer;
	ball_x			:	in		integer;
	ball_y			:	in		integer;
	
	red				:	out	std_logic_vector (7 downto 0);
	gree				:	out	std_logic_vector (7 downto 0);
	blue				:	out	std_logic_vector (7 downto 0)
);
end game_draw;

architecture Behavioral of game_draw is
begin

	drawing : process (display_enable, row, column, paddle_x, paddle_y, ball_x, ball_y)
	begin
			
			if (display_enable = '1') then
				if (row >= paddle_y and column >= paddle_x and column < paddle_x + paddle_width) then
					red <= paddle_red;
					green <= paddle_green;
					red <= paddle_blue;
				elsif (row >= ball_y and row < ball_y + ball_dim and column >= ball_x and column < ball_x + ball_dim) then
					red <= ball_red;
					green <= ball_green;
					blue <= ball_blue;
				else
					red <= bg_red;
					green <= bg_green;
					blue <= bg_blue;
				end if;
			else
				red <= "00000000";
				green <= "00000000";
				blue <= "00000000";
			end if;
	
	end process drawing;



end Behavioral;

