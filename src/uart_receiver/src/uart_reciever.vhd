----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:19:41 11/25/2015 
-- Design Name: 
-- Module Name:    uart_reciever - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_reciever is
port (
	clk 			: in std_logic;		
	serial_in 	: in std_logic;	-- signals from reciever
	baud_rate	: in std_logic;	-- data transmission rate
	data_out		: out std_logic); -- recieved data in vector
end uart_reciever;

architecture Behavioral of uart_reciever is
	variable num_of_zeros : integer := 0;
	variable sample_counter : integer := 0;
	variable bit_counter : integer :=  0;
	variable sample : std_logic;
begin


end Behavioral;

