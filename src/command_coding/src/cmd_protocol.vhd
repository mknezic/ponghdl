----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:54:56 11/23/2015 
-- Design Name: 
-- Module Name:    cmd_protocol - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cmd_protocol is
	port (
	clk 						: in std_logic;
	pause 					: in std_logic;
	left_one_position 	: in std_logic;
	right_one_position 	: in std_logic;
	go 						: out std_logic;
	output_command			: out std_logic_vector(7 downto 0));
end cmd_protocol;

architecture Behavioral of cmd_protocol is

begin


end Behavioral;

