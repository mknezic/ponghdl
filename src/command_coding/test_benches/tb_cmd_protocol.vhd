--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:27:28 11/23/2015
-- Design Name:   
-- Module Name:   D:/Xilinx/Projects Xilinx/command_protocol/tb_cmd_protocol.vhd
-- Project Name:  command_protocol
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: cmd_protocol
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_cmd_protocol IS
END tb_cmd_protocol;
 
ARCHITECTURE behavior OF tb_cmd_protocol IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT cmd_protocol
    PORT(
         clk : IN  std_logic;
         pause : IN  std_logic;
         left_one_position : IN  std_logic;
         right_one_position : IN  std_logic;
         go : OUT  std_logic;
         output_command : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal pause : std_logic := '0';
   signal left_one_position : std_logic := '0';
   signal right_one_position : std_logic := '0';

 	--Outputs
   signal go : std_logic;
   signal output_command : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	
	-- User defined constants
	constant output_pause 	: std_logic_vector(7 downto 0) := "00000001"; 
	constant output_left 	: std_logic_vector(7 downto 0) := "00000010";
	constant output_right 	: std_logic_vector(7 downto 0) := "00000100";
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cmd_protocol PORT MAP (
          clk => clk,
          pause => pause,
          left_one_position => left_one_position,
          right_one_position => right_one_position,
          go => go,
          output_command => output_command
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		
		-- testing pause state
		pause <= '0';
		left_one_position <= '0';
		right_one_position <= '0';
      wait for 100 ns;	
		
		pause <= '1';
		left_one_position <= '0';
		right_one_position <= '0';
      wait for 100 ns;
		
		assert output_command = output_pause report "Output incorrect." severity ERROR;
		
		pause <= '0';
		left_one_position <= '1';
		right_one_position <= '0';
      wait for 100 ns;
		
		pause <= '1';
		left_one_position <= '0';
		right_one_position <= '0';
      wait for 100 ns;
		
		assert output_command = output_pause report "Output incorrect." severity ERROR;
		
		-------------------------------------------------
		-- testing left position state
		
		pause <= '0';
		left_one_position <= '1';
		right_one_position <= '0';
      wait for 100 ns;
		
		assert output_command = output_left report "Output incorrect. Expected" &
		integer'image (to_integer(unsigned(output_pause))) & " was " & integer'image (to_integer(unsigned(output_command))) 
		severity ERROR;
		
		pause <= '0';
		left_one_position <= '1';
		right_one_position <= '0';
      wait for 100 ns;
		
		assert output_command = output_left report "Output incorrect." severity ERROR;
		
		pause <= '0';
		left_one_position <= '0';
		right_one_position <= '0';
      wait for 100 ns;
		
		--------------------------------------------------
		-- testing right position state
		
		pause <= '0';
		left_one_position <= '0';
		right_one_position <= '1';
      wait for 100 ns;
		
		assert output_command = output_right report "Output incorrect." severity ERROR;
		
		pause <= '0';
		left_one_position <= '0';
		right_one_position <= '1';
      wait for 100 ns;
		
		assert output_command = output_right report "Output incorrect." severity ERROR;
		
		pause <= '0';
		left_one_position <= '0';
		right_one_position <= '1';
      wait for 100 ns;
		
		assert output_command = output_right report "Output incorrect." severity ERROR;
		
		pause <= '0';
		left_one_position <= '0';
		right_one_position <= '0';
      wait for 100 ns;
		
      --wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
